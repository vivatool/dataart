<?php

namespace App\Controller;

use App\Entity\Test;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PagesController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @Method({"GET"})
     */
    public function home()
    {
        
        return $this->render('home.html.twig');
    }

    /**
     * @Route("/registration", name="registration")
     * @Method({"GET"})
     */
    public function registration()
    {
        return $this->render('pages/registration.html.twig');
    }

    /**
     * @Route("/faq", name="faq")
     * @Method({"GET"})
     */
    public function faq()
    {
        return $this->render('pages/faq.html.twig');        
    }

    /**
     * @Route("/users", name="users")
     * @Method({"GET"})
     */
    public function users()
    {
        $users = $this->getDoctrine()->getRepository(Test::class)->findAll();

        return $this->render('pages/users.html.twig', [
            'users' => $users
        ]);       
    }

        /**
         * @Route("/users/{id}", name="show_user")
         * @Method({"GET"})
         */
        public function getPersonalPage($id)
        {
            $users = $this->getDoctrine()->getRepository(Test::class)->find($id);

            return $this->render('users/show.html.twig', [
                'users' => $users
            ]);       
        }

    /**
     * @Route("/moderate", name="moderate")
     * @Method({"GET"})
     */
    public function moderate()
    {
        return $this->render('pages/moderate.html.twig');        
    }
}
