<?php

/* layouts/navbar.html.twig */
class __TwigTemplate_d6971a0c2e9212e442a40d344be8423124c29b8baa01ad831707c2a404fa9241 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "layouts/navbar.html.twig"));

        // line 1
        echo "<nav class=\"navbar navbar-expand-sm navbar-dark bg-primary mb-3\">
    <div class=\"container\">
        <a href=\"";
        // line 3
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("homepage");
        echo "\" class=\"navbar-brand\">Marmoset</a>

        <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#mobile-nav\">
            <span class=\"navbar-toggle-icon\"></span>
        </button>

        <div class=\"collapse navbar-collapse\" id=\"mobile-nav\">

            <ul class=\"navbar-nav ml-auto\">
                <li class=\"nav-item\">
                    <a href=\"";
        // line 13
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("homepage");
        echo "\" class=\"nav-link\">Home</a>
                </li>
                <li class=\"nav-item\">
                    <a href=\"";
        // line 16
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("registration");
        echo "\" class=\"nav-link\">Registration</a>
                </li>
                <li class=\"nav-item\">
                    <a href=\"";
        // line 19
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("faq");
        echo "\" class=\"nav-link\">FAQ</a>
                </li>
                <li class=\"nav-item\">
                    <a href=\"";
        // line 22
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("users");
        echo "\" class=\"nav-link\">Users</a>
                </li>
                <li class=\"nav-item\">
                    <a href=\"";
        // line 25
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("moderate");
        echo "\" class=\"nav-link\">Moderate</a>
                </li>
            </ul>

        </div>
    </div>
</nav>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "layouts/navbar.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  67 => 25,  61 => 22,  55 => 19,  49 => 16,  43 => 13,  30 => 3,  26 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<nav class=\"navbar navbar-expand-sm navbar-dark bg-primary mb-3\">
    <div class=\"container\">
        <a href=\"{{ path('homepage') }}\" class=\"navbar-brand\">Marmoset</a>

        <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#mobile-nav\">
            <span class=\"navbar-toggle-icon\"></span>
        </button>

        <div class=\"collapse navbar-collapse\" id=\"mobile-nav\">

            <ul class=\"navbar-nav ml-auto\">
                <li class=\"nav-item\">
                    <a href=\"{{ path('homepage') }}\" class=\"nav-link\">Home</a>
                </li>
                <li class=\"nav-item\">
                    <a href=\"{{ path('registration') }}\" class=\"nav-link\">Registration</a>
                </li>
                <li class=\"nav-item\">
                    <a href=\"{{ path('faq') }}\" class=\"nav-link\">FAQ</a>
                </li>
                <li class=\"nav-item\">
                    <a href=\"{{ path('users') }}\" class=\"nav-link\">Users</a>
                </li>
                <li class=\"nav-item\">
                    <a href=\"{{ path('moderate') }}\" class=\"nav-link\">Moderate</a>
                </li>
            </ul>

        </div>
    </div>
</nav>

", "layouts/navbar.html.twig", "/Users/cosmozoy/Code/DataArt/templates/layouts/navbar.html.twig");
    }
}
